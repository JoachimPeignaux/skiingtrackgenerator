using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;
using UnityEditor;
using UnityEditor.Formats.Fbx.Exporter;

public class TrackViewer : MonoBehaviour
{
    [SerializeField] GameObject doorPrefab;
    [SerializeField] Material redMaterial;
    [SerializeField] Material blueMaterial;
    [SerializeField] Material snowMaterial;
    [SerializeField] Terrain terrain;
    [SerializeField] TerrainGenerator terrainGenerator;

    List<Door> doors = new List<Door>();

    public void SetSize(int size) {
        doors = new List<Door>();
        for (int i = 0; i < size; i++) {
            doors.Add(GameObject.Instantiate(doorPrefab).GetComponent<Door>());
        }
    }

    public void Generate(DoorData[] chromosomes) {
        DoorData[] copy = chromosomes.OrderByDescending(o => o.centerPosition.z).ToArray();
        for (int i = 0; i < doors.Count; i++) {
            doors[i].doorData = copy[i];
            if (i % 2 == 0) {
                doors[i].SetMaterial(blueMaterial);
            } else {
                doors[i].SetMaterial(redMaterial);
            }
            doors[i].UpdatePosition();
        }
    }

    public void SaveTrack() {
        Mesh mesh = new Mesh();
        CombineInstance[] flagMeshes = new CombineInstance[doors.Count*2+1];
        int count = 0;
        for (int i = 0; i < doors.Count; i++)
        {
            foreach (GameObject flag in doors[i].GetFlags())
            {
                flagMeshes[count].mesh = flag.GetComponent<MeshFilter>().sharedMesh;
                flagMeshes[count].transform = Matrix4x4.TRS(flag.transform.position, flag.transform.rotation, flag.transform.localScale);
                count++;
            }
        }
        Mesh slope = terrainGenerator.GenerateMesh(terrain);
        flagMeshes[count].mesh = slope;
        flagMeshes[count].transform = Matrix4x4.TRS(new Vector3(0,0,0), terrain.gameObject.transform.rotation, terrain.gameObject.transform.localScale);
        mesh.CombineMeshes(flagMeshes, true, true);
        mesh.Optimize();
        string saveName = System.DateTime.Now.ToString("s").Replace(':', '-');
        if (mesh) {
            string savePath = "Assets/SavedTracks/" + saveName;
            Debug.Log("Saved Mesh to : " + savePath);
            gameObject.AddComponent<MeshFilter>().mesh = mesh;
            Mesh newMesh = Instantiate(mesh) as Mesh;
            AssetDatabase.CreateAsset(newMesh, savePath + ".asset");
            AssetDatabase.SaveAssets();
            GameObject go = new GameObject();
            go.tag = "Savable";
            go.AddComponent<MeshFilter>().mesh = slope;
            go.AddComponent<MeshRenderer>().material = snowMaterial;
            ModelExporter.ExportObjects(savePath + ".fbx", GameObject.FindGameObjectsWithTag("Savable").Concat( new GameObject[]{go} ).ToArray());
            Destroy(go);
        }
    }
}

using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Door : MonoBehaviour
{
    public DoorData doorData;
    [SerializeField] GameObject leftFlag;
    [SerializeField] GameObject rightFlag;
    [SerializeField] LayerMask terrainLayer;
    const float RAY_DISTANCE = 500;

    public void UpdatePosition() {
        transform.position = new Vector3(doorData.centerPosition.x, 0, doorData.centerPosition.z);
        transform.rotation = Quaternion.AngleAxis(doorData.angleAroundUpAxe, Vector3.up);

        leftFlag.transform.localPosition = new Vector3(-doorData.spacing / 2f, RAY_DISTANCE, 0);
        rightFlag.transform.localPosition = new Vector3(doorData.spacing / 2f, RAY_DISTANCE, 0);
        //left flag
        RaycastHit hit;
        Physics.Raycast(leftFlag.transform.position, Vector3.down, out hit, Mathf.Infinity, terrainLayer);
        if (hit.collider)
            leftFlag.transform.Translate(new Vector3(0, -(hit.distance - transform.localScale.y), 0));
        else
            Debug.Log("Left flag is out of the slop");

        //right flag
        Physics.Raycast(rightFlag.transform.position, Vector3.down, out hit, Mathf.Infinity, terrainLayer);
        if (hit.collider)
            rightFlag.transform.Translate(new Vector3(0, -(hit.distance - transform.localScale.y), 0));
        else
            Debug.Log("right flag is out of the slop");

        doorData.centerPosition.y = (leftFlag.transform.position.y + rightFlag.transform.position.y) / 2f;
    }

    public void SetMaterial(Material material) {
        leftFlag.GetComponent<MeshRenderer>().material = material;
        rightFlag.GetComponent<MeshRenderer>().material = material;
    }

    public GameObject[] GetFlags() {
        return new GameObject[] { leftFlag.gameObject, rightFlag.gameObject};
    }
}

using UnityEngine;
using System.Collections.Generic;

[System.Serializable]
public struct DoorData
{
    public Vector3 centerPosition;
    public float spacing;
    public float angleAroundUpAxe;
    private static readonly float wayPointOffset = 0.7f;

    public DoorData(Vector3 pCenterPosition, float pSpacing, float pAngle)
    {
        centerPosition = pCenterPosition;
        spacing = pSpacing;
        angleAroundUpAxe = (pAngle + 180) % 180;
    }

    public Vector2[] GetFlags(DoorType doorType, float previousDoor_X)
    {
        List<Vector2> flags = new List<Vector2>();
        if (doorType == DoorType.VERTICAL)
        {
            //if (previousDoor_X <= centerPosition.x)
            //{
                flags.Add(new Vector2(centerPosition.x, centerPosition.z + (spacing / 2f)));
                flags.Add(new Vector2(centerPosition.x, centerPosition.z - (spacing / 2f)));
            //}
            //else
            //{
                flags.Add(new Vector2(centerPosition.x, centerPosition.z + (spacing / 2f)));
                flags.Add(new Vector2(centerPosition.x, centerPosition.z - (spacing / 2f)));
            //}
        }
        else
        {
            //if (previousDoor_X <= centerPosition.x)
                flags.Add(new Vector2(
                    centerPosition.x - Mathf.Abs(Mathf.Cos(angleAroundUpAxe * Mathf.Deg2Rad)) * ((spacing / 2.0f)),
                    centerPosition.z - Mathf.Abs(Mathf.Sin(angleAroundUpAxe * Mathf.Deg2Rad)) * ((spacing / 2.0f))));
            //else
                flags.Add(new Vector2(
                   centerPosition.x + Mathf.Abs(Mathf.Cos(angleAroundUpAxe * Mathf.Deg2Rad)) * ((spacing / 2.0f)),
                   centerPosition.z - Mathf.Abs(Mathf.Sin(angleAroundUpAxe * Mathf.Deg2Rad)) * ((spacing / 2.0f))));
        }
        return flags.ToArray();
    }

    public Vector2[] GetWayPoints(DoorType doorType, float previousDoor_X)
    {
        List<Vector2> waypoints = new List<Vector2>();
        if (doorType == DoorType.VERTICAL)
        {
            if (previousDoor_X <= centerPosition.x)
            {
                waypoints.Add(new Vector2(centerPosition.x + wayPointOffset, centerPosition.z + (spacing / 2f)));
                waypoints.Add(new Vector2(centerPosition.x - wayPointOffset, centerPosition.z - (spacing / 2f)));
            }
            else
            {
                waypoints.Add(new Vector2(centerPosition.x - wayPointOffset, centerPosition.z + (spacing / 2f)));
                waypoints.Add(new Vector2(centerPosition.x + wayPointOffset, centerPosition.z - (spacing / 2f)));
            }
        }
        else
        {
            if (previousDoor_X <= centerPosition.x)
                waypoints.Add( new Vector2(
                    centerPosition.x - Mathf.Abs(Mathf.Cos(angleAroundUpAxe * Mathf.Deg2Rad)) * ((spacing / 2.0f) - wayPointOffset),
                    centerPosition.z - Mathf.Abs(Mathf.Sin(angleAroundUpAxe * Mathf.Deg2Rad)) * ((spacing / 2.0f) - wayPointOffset)
                    ));
            else
                waypoints.Add(new Vector2(
                   centerPosition.x + Mathf.Abs(Mathf.Cos(angleAroundUpAxe * Mathf.Deg2Rad)) * ((spacing / 2.0f) - wayPointOffset),
                   centerPosition.z - Mathf.Abs(Mathf.Sin(angleAroundUpAxe * Mathf.Deg2Rad)) * ((spacing / 2.0f) - wayPointOffset)
                   ));
        }
        return waypoints.ToArray();
    }
}

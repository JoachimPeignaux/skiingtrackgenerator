using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SkierPhysics : MonoBehaviour
{

    [SerializeField] private Terrain t;
    [SerializeField] private LayerMask terrain;
    [SerializeField] private GameObject startPoint; //point de d�part du skieur
    [SerializeField] private float deltaTime; //temps entre deux calculs

    //used for calculs
    private Vector3 position;
    private Vector3 force;
    private Vector3 vitesse;
    private Vector3 acceleration;
    private Vector3 normal;
    private Vector3 gravity = Physics.gravity;
    private Vector3 frottement;
    private Vector3 trainee;
    private Vector2 nextWaypoint;
    private Vector2 nextLeftFlag;
    private Vector2 nextRightFlag;

    private int indexOfWaypoint;
    private int indexOfLeftFlag;
    private int indexOfRightFlag;
    private float score;

    public void Reset()
    {
        position = startPoint.transform.position;

        force = Vector3.zero;
        vitesse = Vector3.zero;
        acceleration = Vector3.zero;
        normal = Vector3.zero;
        frottement = Vector3.zero;
        trainee = Vector3.zero;

        indexOfWaypoint = 0;
        indexOfLeftFlag = 0;
        indexOfRightFlag = 1;
        score = 0f;
        nextWaypoint = Vector2.zero;
        nextLeftFlag = Vector2.zero;
        nextRightFlag = Vector2.zero;
    }

    public float Evaluate(List<Vector2> waypoints, List<Vector2> flags, bool drawPath = false) // mettre drawPath a true seulement quand on veut afficher le trajet, donc le faire que pour la derni�re �ventuellement
    {
        Reset();
        nextWaypoint = waypoints[0];
        nextLeftFlag = flags[0];
        nextRightFlag = flags[1];

        Vector2 positionV2 = new Vector2(position.x, position.z);

        while (indexOfWaypoint < waypoints.Count)
        {
            normal = Vector3.zero;
            frottement = Vector3.zero;
            force = gravity ;

            if (position.z < nextWaypoint.y)
            {
                if ((positionV2 - nextLeftFlag).magnitude < (nextLeftFlag - nextRightFlag).magnitude &&
                    (positionV2 - nextRightFlag).magnitude < (nextLeftFlag - nextRightFlag).magnitude)
                {
                    score++;
                }

                indexOfWaypoint++;
                indexOfLeftFlag += 2;
                indexOfRightFlag += 2;
                if (indexOfWaypoint == waypoints.Count)
                {
                    //Debug.Log("nb de portes ok : " + score);
                    score = score / waypoints.Count;
                    //Debug.Log("total waypoint : " + waypoints.Count);
                    break;
                }
                    
                nextWaypoint = waypoints[indexOfWaypoint];
                nextLeftFlag = flags[indexOfLeftFlag];
                nextRightFlag = flags[indexOfRightFlag];
            }

            //calcule de la normal
            float distanceWithGround = position.y - t.SampleHeight(position);
            position.y = t.SampleHeight(position) + 1;
            Vector3 b = position + new Vector3(0.1f, 0, 0.1f);
            Vector3 c = position + new Vector3(-0.1f, 0, 0.1f);
            b.y = t.SampleHeight(b);
            c.y = t.SampleHeight(c);
            normal = Vector3.Cross(c, b).normalized * force.magnitude ;
            force += normal;

            //frottement ski-neige
            frottement = -force.normalized * force.magnitude * 0.150f; //0.150 est le coefficicent de frottement de la neige

            //calcul de la trainee (penetration dans l'air)
            trainee = -vitesse.normalized * (0.25f * vitesse.magnitude * vitesse.magnitude);  //Pour un skieur de 1,65 m en position de l'�uf, kS est voisin de 0,25 N.s2.m-2
      
            //ajout de toutes les forces
            force += frottement + trainee;

            //calcule de l'acc�l�ration subit : F = m*a
            acceleration = force / 60f; //60kg, poid id�al d'un homme de 1m65 selon l'IMC

            //calcul de la vitesse : v = a * t
            vitesse += (acceleration * deltaTime); //+= pour avoir de l'inertie

            //tourne le vecteur vitesse autour de l'axe y pour viser le prochain waypoints
            Vector3 direction = new Vector3(nextWaypoint.x, 0, nextWaypoint.y) - position;
            direction.y = vitesse.y;

            Quaternion rot;
            if ((direction.normalized-vitesse.normalized).magnitude < 0.2f)
            {
                rot = Quaternion.identity;
            }
            else
            {
                rot = Quaternion.FromToRotation(vitesse, direction);
            }

            float oldY = vitesse.y;
            vitesse = Quaternion.RotateTowards(Quaternion.identity, rot, 45*deltaTime) * vitesse; //ici 45 veut dire que en 1s, le skieur peut tourner de 45degr�s max
            vitesse.y = oldY;

            //maj de la position
            position += vitesse * deltaTime;

            if (drawPath) {
                GameObject sph = GameObject.CreatePrimitive(PrimitiveType.Sphere);
                sph.transform.position = position + new Vector3(0, 1.5f, 0);
            }
        }
        return score;
    }
}

//acceleration = force / masse. Force c'est gravit�, normale, frottement
//vitesse = acceleration * temps

using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;
using System.Linq;

using Random = UnityEngine.Random;

enum Direction { NONE, LEFT, RIGHT, FORWARD };
public enum DoorType { NONE, HORIZONTAL, VERTICAL, INCLINED_LOOKING_LEFT, INCLINED_LOOKING_RIGHT };

//peut etre optimis�
public class Chromosome {
    public DoorData[] data;
    public float score;

    public Chromosome(DoorData[] pData, float pScore) {
        data = pData;
        score = pScore;
    }
}

public class GeneticAlgorithm : MonoBehaviour
{
    [SerializeField, Range(0f, 1f)] float scoreToReach = 0.98f;

    [Header("DoorData generation limit variables")]
    [SerializeField] float minSpacing;
    [SerializeField] float maxSpacing;
    [SerializeField] float minAngle;
    [SerializeField] float maxAngle;
    [SerializeField] Vector2 topLeft;
    [SerializeField] Vector2 bottomRight;

    [Header("Track variables")]
    TrackViewer track;
    [SerializeField] int numberOfDoors;

    [Header("Genetic variables")]
    [SerializeField] int populationSize;
    List<Chromosome> chromosomes;
    [SerializeField] AnimationCurve mutateProbabilityCurve;
    [SerializeField] AnimationCurve killProbabilityCurve;
    [SerializeField] AnimationCurve reproductionProbabilityCurve;

    [Header("Mutation variables")]
    [SerializeField] float minSpacingMutation;
    [SerializeField] float maxSpacingMutation;
    [SerializeField] float minRotationOffset;
    [SerializeField] float maxRotationOffset;
    [SerializeField] float minTranslationOffset;
    [SerializeField] float maxTranslationOffset;

    [Header("Evaluation variables")]
    [SerializeField] float horizontalHalfAngleAllowed;
    [SerializeField] float verticalHalfAngleAllowed;
    [SerializeField] float inclinedHalfAngleAllowed;
    [SerializeField] float forwardOffsetAllowed;
    [SerializeField] float minDistanceBetween2Verticals;
    [SerializeField] float maxDistanceBetween2Verticals;
    [SerializeField] float minDistanceBetween2Doors;
    [SerializeField] float maxDistanceBetween2Doors;
    [SerializeField] bool usingSkierPhysicsSim = false;
    [SerializeField] SkierPhysics skierPhysics;

    DoorType[] doorTypes;

    private void Start() {
        doorTypes = new DoorType[numberOfDoors];
        track = GetComponent<TrackViewer>();
        track.SetSize(numberOfDoors);

        chromosomes = new List<Chromosome>();
        Random.InitState((int)System.DateTime.Now.Ticks);

       StartCoroutine(MainBoucle());
    }

    IEnumerator MainBoucle() //la coroutine permet de pouvoir afficher la meilleur track en cour, ne pas avoir a attendre que la boucle while soit finie
    {
        //INITIALISE population with random candidate solutions
        Initialise();

        //EVALUATE each candidate
        Evaluate();

        //REPEAT UNTIL (TERMINATION CONDITION is satisfied)
        long countSim = 0;
        while (chromosomes[0].score < scoreToReach) {//0.98 at least
            //SELECT PARENTS
            List<Tuple<int, int>> parents = SelectParents(chromosomes.Count*3);
            
            //RECOMBINE pairs of parents
            RecombineParents(parents);

            //MUTATE the new candidates
            MutateCandidates();

            //EVALUATE new candidates
            Evaluate();

            //SELECT individuals for the next generation, KILL the other ones
            SelectIndividuals();

            //PRINT the result, more for debug
            Debug.Log("best score : " + chromosomes[0].score);
            track.Generate(chromosomes[0].data);
            yield return null;
        }
        Debug.Log(countSim + "  simulations");
        track.SaveTrack(); //can't generate a mesh from a terrain in Unity, will have to do it using the heighMap
             
        //code pour avoir la liste des waypoints
        List<Vector2> waypoints = new List<Vector2>();
        List<Vector2> flags = new List<Vector2>();
        float previousX = 0;
        for (int i = 0; i < chromosomes[0].data.Length; i++)  {
            Vector2[] newWayPoints = chromosomes[0].data[i].GetWayPoints(doorTypes[i], previousX);
            Vector2[] newFlags = chromosomes[0].data[i].GetFlags(doorTypes[i], previousX);
            previousX = newWayPoints[0].x;
            waypoints.AddRange(newWayPoints);
            flags.AddRange(newFlags);
        }
        skierPhysics.Evaluate(waypoints,flags, true);
    }

    void Initialise() {
        for (int p = 0; p < populationSize; p++) {
            DoorData[] track = new DoorData[numberOfDoors];
            for (int i = 0; i < numberOfDoors; i++) {
                track[i] = new DoorData(new Vector3(Random.Range(topLeft.x, bottomRight.x), 0, Random.Range(topLeft.y, bottomRight.y)), Random.Range(minSpacing, maxSpacing), Random.Range(minAngle, maxAngle));
            }
            chromosomes.Add(new Chromosome(track.OrderByDescending(o => o.centerPosition.z).ToArray(), -1));
        }
    }

    void Evaluate() {
        foreach (Chromosome chromosome in chromosomes) {
            if (chromosome.score == -1) {

                chromosome.data = chromosome.data.OrderByDescending(o => o.centerPosition.z).ToArray();

                //40m de largeur
                float scoreLargeur = 0;
                {
                    float minX = chromosome.data.Min(o => o.centerPosition.x);
                    float maxX = chromosome.data.Max(o => o.centerPosition.x);
                    float largeur = Mathf.Clamp(maxX - minX, 0, 80);

                    if (largeur <= 40)
                        scoreLargeur = (largeur / 40f);
                    else
                        scoreLargeur = 2 - (largeur / 40f);
                }

                //defini le type de chaque porte selon l'angle
                for (int iDoor = 0; iDoor < numberOfDoors; iDoor++) {
                    if ((chromosome.data[iDoor].angleAroundUpAxe >= (180 - horizontalHalfAngleAllowed)) || (chromosome.data[iDoor].angleAroundUpAxe <= horizontalHalfAngleAllowed))
                    {
                        doorTypes[iDoor] = DoorType.HORIZONTAL;
                    }
                    else if ((chromosome.data[iDoor].angleAroundUpAxe >= (90 - verticalHalfAngleAllowed)) && (chromosome.data[iDoor].angleAroundUpAxe <= (90 + verticalHalfAngleAllowed)))
                    {
                        doorTypes[iDoor] = DoorType.VERTICAL;
                    }
                    else if ((chromosome.data[iDoor].angleAroundUpAxe >= (45 - inclinedHalfAngleAllowed)) && (chromosome.data[iDoor].angleAroundUpAxe <= (45 + inclinedHalfAngleAllowed)))
                    {
                        doorTypes[iDoor] = DoorType.INCLINED_LOOKING_LEFT;
                    }
                    else if ((chromosome.data[iDoor].angleAroundUpAxe >= (135 - inclinedHalfAngleAllowed)) && (chromosome.data[iDoor].angleAroundUpAxe <= (135 + inclinedHalfAngleAllowed)))
                    {
                        doorTypes[iDoor] = DoorType.INCLINED_LOOKING_RIGHT;
                    }
                    else
                    {
                        doorTypes[iDoor] = DoorType.NONE;
                    }
                }

                //entre 2portes successives : 6 � 13m vertical
                float scoreDistance = 0;
                for (int iDoor = 0; iDoor < numberOfDoors - 1; iDoor++)
                {
                    Vector2 position1 = new Vector2(chromosome.data[iDoor].centerPosition.y, chromosome.data[iDoor].centerPosition.z);
                    Vector2 position2 = new Vector2(chromosome.data[iDoor+1].centerPosition.y, chromosome.data[iDoor+1].centerPosition.z);
                    float distance = (position2 - position1).magnitude;
                    if (doorTypes[iDoor] == DoorType.VERTICAL && doorTypes[iDoor + 1] == DoorType.VERTICAL)
                    {
                        distance -= ((chromosome.data[iDoor].spacing + chromosome.data[iDoor+1].spacing) / 2f);

                        if (distance >= minDistanceBetween2Verticals && distance <= maxDistanceBetween2Verticals)
                            scoreDistance += 1f / (numberOfDoors - 1f);
                        else
                        {
                            if (distance < minDistanceBetween2Verticals)
                                scoreDistance += (distance / minDistanceBetween2Verticals) / (numberOfDoors - 1f);
                            else
                                scoreDistance += Mathf.Clamp01(-(1f / minDistanceBetween2Verticals) * distance + (minDistanceBetween2Verticals+maxDistanceBetween2Verticals)/minDistanceBetween2Verticals) / (numberOfDoors - 1f);
                        }
                    }
                    else {
                        if (distance >= minDistanceBetween2Doors && distance <= maxDistanceBetween2Doors)
                            scoreDistance += 1f / (numberOfDoors - 1f);
                        else
                        {
                            if (distance < minDistanceBetween2Doors)
                                scoreDistance += (distance / minDistanceBetween2Doors) / (numberOfDoors - 1f);
                            else
                                scoreDistance += Mathf.Clamp01(-(1f / minDistanceBetween2Doors) * distance + (minDistanceBetween2Doors+maxDistanceBetween2Doors / minDistanceBetween2Doors)) / (numberOfDoors - 1f);
                        }
                    }
                }

                //1 a 3 chicanes de 3 ou 4 portes
                int numberOfChicanes = 0;
                int maxLenght = 0;

                for (int iDoor = 0; iDoor < numberOfDoors; iDoor++) {
                    int lengthOfChicane = 0;

                    while( iDoor + lengthOfChicane < numberOfDoors && lengthOfChicane<4) {
                        if (doorTypes[iDoor + lengthOfChicane] ==  DoorType.HORIZONTAL)
                            lengthOfChicane++;
                        else
                            break;
                    }
                    iDoor += lengthOfChicane;
                    if (lengthOfChicane > maxLenght)
                    {
                        maxLenght = lengthOfChicane;
                    }
                    if (lengthOfChicane >= 3)
                    {
                        numberOfChicanes++;
                    }
                }

                float scoreStandard = 0;
                if (numberOfChicanes != 0 && numberOfChicanes <= 3) {
                    scoreStandard = 1f;
                }
                else
                {
                    if (numberOfChicanes == 0) // 0:0pt, 1: 1/3pt, 2:2/3pt
                        scoreStandard = (maxLenght/3f);
                    else if (numberOfChicanes < 7)// 6: 0pt, 5: 1/3pt, 4: 2/3pt
                        scoreStandard = (6f - numberOfChicanes) / 3f;
                }

                //3 double minimum
                int numberOfDouble = 0;
                float smallBonusScore = 0;
                for (int iDoor = 0; iDoor < numberOfDoors-1; iDoor++) {
                    DoorData door1 = chromosome.data[iDoor];
                    DoorData door2 = chromosome.data[iDoor+1];

                    float checkedCondition = 0;

                    if (doorTypes[iDoor]==DoorType.VERTICAL && doorTypes[iDoor+1] == DoorType.VERTICAL)
                    {
                        checkedCondition++;
                        if (Mathf.Abs(door1.centerPosition.x - door2.centerPosition.x) < forwardOffsetAllowed)
                            checkedCondition++;
                        float distance = (door1.centerPosition.z - door2.centerPosition.z) - ((door1.spacing + door2.spacing) / 2f);
                        if (Mathf.Abs(distance) >= minDistanceBetween2Verticals && Mathf.Abs(distance) <= maxDistanceBetween2Verticals ) {
                            checkedCondition++;
                        }
                    }

                    if (checkedCondition == 3)
                        numberOfDouble++;
                    else
                        smallBonusScore += (checkedCondition / 3f )/(float)(numberOfDoors-1);

                }
                float scoreDouble = 0;
                if (numberOfDouble >= 3)
                {
                    scoreDouble = 1f;
                }
                else if (numberOfDouble < 3) //0: 0pt, 1: 1/3pt, 2: 2/3pt
                {
                    scoreDouble = numberOfDouble / 3f;
                    if (scoreDouble == 0.0f) {
                        scoreDouble = smallBonusScore / 2f;
                    }
                }

                //1 � 3banane
                float scoreBanana = 0;
                int nbrBanane = 0;
                for (int iDoor = 0; iDoor < numberOfDoors-2; iDoor++) {
                    if(doorTypes[iDoor] == DoorType.HORIZONTAL)
                    {
                        if (chromosome.data[iDoor].centerPosition.x < chromosome.data[iDoor + 1].centerPosition.x)
                        {
                            //Direction.RIGHT;
                            if (doorTypes[iDoor + 1] == DoorType.INCLINED_LOOKING_RIGHT && (iDoor + 2 >= numberOfDoors || doorTypes[iDoor + 2] != DoorType.INCLINED_LOOKING_RIGHT && doorTypes[iDoor + 2] != DoorType.INCLINED_LOOKING_LEFT) )
                            {
                                nbrBanane++;
                            }
                        }
                        else
                        {
                            //Direction.LEFT;
                            if (doorTypes[iDoor + 1] == DoorType.INCLINED_LOOKING_LEFT && (iDoor + 2 >= numberOfDoors || doorTypes[iDoor + 2] != DoorType.INCLINED_LOOKING_RIGHT && doorTypes[iDoor + 2] != DoorType.INCLINED_LOOKING_LEFT))
                            {
                                nbrBanane++;
                            }
                        }
                    }
                }

                if (nbrBanane > 0 && nbrBanane <= 3)
                    scoreBanana = 1;
                else
                    if (nbrBanane < 7 && nbrBanane > 0)
                    scoreBanana = (6 - nbrBanane)/3f;

                //le taux de r�ussite
                List<Vector2> waypoints = new List<Vector2>();
                List<Vector2> flags = new List<Vector2>();
                float previousX = 0;
                for (int i = 0; i < chromosome.data.Length; i++)
                {
                    Vector2[] newWayPoints = chromosome.data[i].GetWayPoints(doorTypes[i], previousX);
                    Vector2[] newFlags = chromosome.data[i].GetFlags(doorTypes[i], previousX);
                    previousX = newWayPoints[0].x;
                    waypoints.AddRange(newWayPoints);
                    flags.AddRange(newFlags);
                }

                float scoreSkier = 1;
                if (usingSkierPhysicsSim)
                {
                    scoreSkier = skierPhysics.Evaluate(waypoints, flags);
                }
                

                //alternance g-d
                float scoreAlternance = 0;

                Direction previousDirection = Direction.NONE;
                for (int iDoor = 0; iDoor < numberOfDoors-1; iDoor++)
                {
                    Direction currentDirection;
                    if (chromosome.data[iDoor].centerPosition.x < chromosome.data[iDoor + 1].centerPosition.x - forwardOffsetAllowed)
                        currentDirection = Direction.RIGHT;
                    else if (chromosome.data[iDoor].centerPosition.x > chromosome.data[iDoor + 1].centerPosition.x + forwardOffsetAllowed)
                        currentDirection = Direction.LEFT;
                    else
                        currentDirection = Direction.FORWARD;

                    switch (doorTypes[iDoor]) {
                        case DoorType.INCLINED_LOOKING_RIGHT :
                            if (currentDirection == Direction.RIGHT && previousDirection == Direction.RIGHT)
                                scoreAlternance = 1f / (numberOfDoors);
                            else
                                scoreAlternance = 0;
                            break;
                        case DoorType.INCLINED_LOOKING_LEFT:
                            if (currentDirection == Direction.LEFT && previousDirection == Direction.LEFT)
                                scoreAlternance = 1f / (numberOfDoors);
                            else
                                scoreAlternance = 0;
                            break;
                        case DoorType.VERTICAL:
                            if (previousDirection != Direction.FORWARD) //debut de la chaine de vertical
                                scoreAlternance += (currentDirection == Direction.FORWARD) ? 1f / (numberOfDoors) : 0;
                            else
                            {
                                if (currentDirection == Direction.FORWARD) //milieu de chaine
                                    scoreAlternance += 1f / (numberOfDoors);
                                else //fin de chaine, regarder la parit� de la longueur de la chaine
                                {
                                    int sizeOfQueue = 1; //don't take the head of the thread in count
                                    int jDoor = iDoor - 1;
                                    while (jDoor > 0 && (chromosome.data[jDoor - 1].centerPosition.x - chromosome.data[jDoor].centerPosition.x) <= forwardOffsetAllowed) {
                                        sizeOfQueue++;
                                        jDoor--;
                                    }
                                    jDoor++;

                                    Direction enterDirection;
                                    if (chromosome.data[jDoor-1].centerPosition.x < chromosome.data[jDoor].centerPosition.x - forwardOffsetAllowed)
                                        enterDirection = Direction.RIGHT;
                                    else if (chromosome.data[jDoor-1].centerPosition.x > chromosome.data[jDoor].centerPosition.x + forwardOffsetAllowed)
                                        enterDirection = Direction.LEFT;
                                    else
                                        enterDirection = Direction.FORWARD;

                                    if (sizeOfQueue % 2 == 0)
                                    {
                                        if (currentDirection == enterDirection)
                                            scoreAlternance += 0;
                                        else
                                            scoreAlternance += 1f / (numberOfDoors);
                                    }
                                    else
                                    {
                                        if (currentDirection == enterDirection || enterDirection == Direction.FORWARD)
                                            scoreAlternance += 1f / (numberOfDoors);
                                        else
                                            scoreAlternance += 0;
                                    }
                                }
                            }        
                            break;
                        case DoorType.HORIZONTAL:
                            if ( doorTypes[iDoor+1] == DoorType.INCLINED_LOOKING_LEFT || doorTypes[iDoor + 1] == DoorType.INCLINED_LOOKING_RIGHT)
                                scoreAlternance += (currentDirection == previousDirection) ? 1f / (numberOfDoors) : 0;
                            else
                                scoreAlternance += (currentDirection != previousDirection) ? 1f / (numberOfDoors) : 0;
                            break;
                        case DoorType.NONE:
                            //scoreAlternance += (currentDirection != previousDirection) ? 1f / (numberOfDoors - 1) : 0;
                            break;
                        default:
                            break;
                    }
                    previousDirection = currentDirection;
                }

                //si le dernier est inclin� ou droit
                if (previousDirection == Direction.RIGHT && doorTypes[numberOfDoors-1] == DoorType.INCLINED_LOOKING_RIGHT)
                    scoreAlternance += 1f / (numberOfDoors - 1);
                else if (previousDirection == Direction.LEFT && doorTypes[numberOfDoors - 1] == DoorType.INCLINED_LOOKING_LEFT)
                    scoreAlternance += 1f / (numberOfDoors - 1);
                else if (doorTypes[numberOfDoors - 1] == DoorType.HORIZONTAL)
                    scoreAlternance += 1f / (numberOfDoors - 1);


                //Calcul du score final de la track
                if(usingSkierPhysicsSim)
                {
                    chromosome.score = (scoreDistance + scoreAlternance + scoreLargeur + scoreStandard + scoreDouble * 2 + scoreBanana + scoreSkier * 2) / 9f;
                } else
                {
                    chromosome.score = (scoreDistance + scoreAlternance + scoreLargeur + scoreStandard + scoreDouble * 2 + scoreBanana) / 7f;
                }
                
                if (scoreDistance == 0f || scoreAlternance == 0f || scoreLargeur == 0f || scoreStandard == 0f || scoreDouble == 0f || scoreBanana == 0f || scoreSkier == 0f) //pour garantir l'�quilibrage
                    chromosome.score /= 2f;
            }
        }
        chromosomes = chromosomes.OrderByDescending(o => o.score).ToList();
    }

    List<Tuple<int, int>> SelectParents(int numberOfCouple) {
        List<Tuple<int, int>> parents = new List<Tuple<int, int>>(numberOfCouple);
        for (int i = 0; i < numberOfCouple/3; i++)
        {
            float t = Random.Range(0f, 1f);
            int parent1 = Mathf.RoundToInt(reproductionProbabilityCurve.Evaluate(t) * (chromosomes.Count-1));
            t = Random.Range(0f, 1f);
            int parent2 = Mathf.RoundToInt(reproductionProbabilityCurve.Evaluate(t) * (chromosomes.Count-1));
            parents.Add(new Tuple<int, int>(parent1, parent2));
        }
        return parents;
    }

    void RecombineParents(List<Tuple<int, int>> parents) {
        foreach (Tuple<int, int> tuple in parents) {
            DoorData[] track1 = new DoorData[numberOfDoors];
            DoorData[] track2 = new DoorData[numberOfDoors];
            for (int i = 0; i < numberOfDoors; i++) {
                if (Random.Range(0,2) == 0 )
                {
                    track1[i] = chromosomes[tuple.Item1].data[i];
                    track2[i] = chromosomes[tuple.Item2].data[i];
                }
                else
                {
                    track1[i] = chromosomes[tuple.Item2].data[i];
                    track2[i] = chromosomes[tuple.Item1].data[i];
                }
            }
            chromosomes.Add(new Chromosome(track1, -1));
            chromosomes.Add(new Chromosome(track2, -1));
        }
    }

    void MutateCandidates() {
        for (float i = 1; i < chromosomes.Count; i++)
        {
            float r = Random.Range(0f, 1f);
            if (r < mutateProbabilityCurve.Evaluate(i / (float)chromosomes.Count))
            {
                int numberOfMutations = Random.Range(1, Mathf.CeilToInt(numberOfDoors/3));
                for (int mutation = 0; mutation < numberOfMutations; mutation++)
                {
                    int mutatedDoor = Random.Range(0, numberOfDoors);
                    Mutate(ref chromosomes[(int)i].data[mutatedDoor]);
                }
            }
        }
    }

    void Mutate(ref DoorData data) {
        int r = Random.Range(0, 3);
        switch (r)
        {
            case 0: //rotate
                int signe0 = Random.Range(0, 2);
                float angle = Random.Range(minRotationOffset, maxRotationOffset);
                data.angleAroundUpAxe = (data.angleAroundUpAxe+((signe0==0)? angle : -angle)+180)%180;
        
                return;
            case 1: //spacing
                int signe1 = Random.Range(0, 2);
                float offset = Random.Range(minSpacingMutation, maxSpacingMutation);
                data.spacing += (signe1 == 0) ? offset : -offset;
                data.spacing = Mathf.Clamp(data.spacing, minSpacing, maxSpacing);
                return;
            case 2: //translation
                Vector3 positionOffset = Random.onUnitSphere;
                positionOffset.y = 0;
                positionOffset.Normalize();
                float offsetSize = Random.Range(minTranslationOffset, maxTranslationOffset);
                positionOffset *= offsetSize;
                data.centerPosition += new Vector3(positionOffset.x, 0, positionOffset.z);
                return;
            default:
                return;
        }
    }

    void SelectIndividuals() {
        while (chromosomes.Count > populationSize) {
            float t = Random.Range(0f, 1f);
            t = killProbabilityCurve.Evaluate(t);
            chromosomes.RemoveAt(Mathf.CeilToInt(t * (float)(chromosomes.Count - 2)));
            if (Mathf.CeilToInt(t * (float)(chromosomes.Count - 2)) == 0) {
                Debug.LogError("Best candidat is dead");
            }
        }
    }

}

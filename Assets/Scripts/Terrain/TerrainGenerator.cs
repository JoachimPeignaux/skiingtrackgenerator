using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TerrainGenerator : MonoBehaviour
{
    const int step = 3;
    public Mesh GenerateMesh(Terrain terrain) {
        //generate vertices
        Vector3 origin = terrain.transform.position;

        List<Vector3> vertices = new List<Vector3>();
        for (int x = (int)origin.x; x < origin.x+terrain.terrainData.size.x; x+= step)
        {
            for (int y = (int)origin.z; y < origin.z + terrain.terrainData.size.z; y+= step)
            {
                Vector3 topLeft = new Vector3    (x     , terrain.SampleHeight(new Vector3(x     , 0, y     )), y     );
                Vector3 bottomLeft = new Vector3 (x     , terrain.SampleHeight(new Vector3(x     , 0, y+step)), y+step);
                Vector3 topRight = new Vector3   (x+step, terrain.SampleHeight(new Vector3(x+step, 0, y     )), y     );
                Vector3 bottomRight = new Vector3(x+step, terrain.SampleHeight(new Vector3(x+step, 0, y+step)), y+step);

                vertices.Add(topLeft);
                vertices.Add(bottomLeft);
                vertices.Add(bottomRight);
                vertices.Add(topRight);
            }
        }

        //generate tris
        List<int> tris = new List<int>();
        for (int i = 0; i < vertices.Count; i += 4)
        {
            tris.Add(i); tris.Add(i + 2); tris.Add(i + 3);
            tris.Add(i); tris.Add(i + 1); tris.Add(i + 2);
        }

        //create and return the mesh
        Mesh slope = new Mesh();
        slope.Clear();
        slope.vertices = vertices.ToArray();
        slope.triangles = tris.ToArray();
        slope.RecalculateNormals();
        return slope;
    }

}
